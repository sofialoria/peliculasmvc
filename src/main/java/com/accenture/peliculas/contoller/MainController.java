package com.accenture.peliculas.contoller;

import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.accenture.peliculas.model.Estudio;
import com.accenture.peliculas.model.InputPelicula;
import com.accenture.peliculas.model.Pelicula;
import com.accenture.peliculas.model.Usuario;
import com.accenture.peliculas.repository.DaoEstudio;
import com.accenture.peliculas.repository.DaoPelicula;


@RestController
//@RequestMapping("/buster")
public class MainController {

	@Autowired
	private DaoPelicula pelidao;
	
	@Autowired
	private DaoEstudio estudiodao;
	
	@GetMapping()
	public ModelAndView Iniciar() {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("principal");//html
		return mav;
	
	}
	
	
	
//	@CrossOrigin(origins = "http://localhost:4200")
//	@PostMapping(path= {"/createfilm"})
//	public Pelicula crearPelicula(@RequestBody Pelicula peli) {
//	//al crear la pelicula le tengo que asignar un id de estudio
//	//donde guardarlo, lo paso como parametro
//		return pelidao.save(peli);
//
//	}
	
	//Crearfilm corregido
	//al crear la pelicula le tengo que asignar un id de estudio
	//donde guardarlo, lo paso como parametro
	@PostMapping(path= {"{id}/createfilm"})
	public Pelicula crearPelicula(@PathVariable("id") Long idest,@RequestBody Pelicula peli) {

		Estudio est = estudiodao.findById(idest).orElse(null);
		
		Pelicula movie = new Pelicula();		
		
		if(est!= null) {
			
			//si el estudio existe le seteo la pelicula(la creo)
			movie.setEstudio(est); //le seteo ese idestudio
			movie.setNombre(peli.getNombre());
			movie.setDescripcion(peli.getDescripcion());
			movie.setFoto(peli.getFoto());
			movie.setAnio(peli.getAnio());
			movie.setDirector(peli.getDirector());
			movie.setId(idest); //le seteo este id de estudio
	
		}
		
		return pelidao.save(movie);
	}
	
//	@CrossOrigin(origins = "http://localhost:4200")
	@PostMapping(path= {"/createstudio"})
	public Estudio crearEstudio(@RequestBody Estudio estu) {
		
		return estudiodao.save(estu);
	}
	
	
	//nuevo estudio MAV
	@GetMapping("/newStudy")
	public ModelAndView crearNuevoEstudio(Model model) {
		Estudio estu = new Estudio();
		model.addAttribute("estudio", estu);
		ModelAndView mav = new ModelAndView();
		
		mav.setViewName("nuevoestudio"); //html
		
		return mav; 
	}
	
	//guardar el estudio creado
	@GetMapping(value = "/guardarEstudio")
	public String guardarEstudio(@ModelAttribute("estudio") Estudio estudio) {
		estudiodao.save(estudio);
		return "redirect:/";
	}

	
	//crea pelicula c model
	@GetMapping("/newFilm")
	public String crearNuevaPelicula(Model model) {
		InputPelicula peli = new InputPelicula();
		model.addAttribute("inputpelicula", peli);
		
		return "nuevapeli"; //html
	}
	
	
	//guardar pelicula creada
	@PostMapping(value = "/guardarPeli")
	public String guardarPeli(@ModelAttribute("pelicula") InputPelicula pelicula) {
	Estudio est = estudiodao.findById(pelicula.getIdEstudio()).orElse(null);
		
		Pelicula movie = new Pelicula();		
		
		if(est!= null) {
			
			//si el estudio no existe le seteo la pelicula(la creo)
			movie.setEstudio(est); //le seteo ese idestudio
			movie.setNombre(pelicula.getPeli().getNombre());
			movie.setDescripcion(pelicula.getPeli().getDescripcion());
			movie.setFoto(pelicula.getPeli().getFoto());
			movie.setAnio(pelicula.getPeli().getAnio()); 
			movie.setDirector(pelicula.getPeli().getDirector());
			movie.setId(est.getId()); //le seteo este id de estudio

			pelidao.save(movie);
		}
		return "redirect:/";
	}
	
	
	//trae la lista de peliculas mientras haya en la
	//lista me muestra la data de cada pelicula del array
	//	@CrossOrigin(origins = "http://localhost:4200")
//	@GetMapping(path= {"{id}/peliculas"})
//	public ResponseEntity<Object> listarPeliculas(@PathVariable("id") Long idest){
//	//PARAMETRO ID DEL ESTUDIO PARA ENCONTRAR LAS PELICULAS DE ESE ESTUDIO
//	
//		Estudio est = estudiodao.findById(idest).orElse(null);
//		
//		List<Pelicula> peliculasFound = pelidao.findByEstudio(idest);
//	
//		JSONObject obj = new JSONObject();
//		
//		if(!peliculasFound.isEmpty()) {
//			
//			JSONArray jugArray = new JSONArray();
//
//			for(Pelicula auxp: peliculasFound) {
//			
//				JSONObject jaux = new JSONObject();
//				jaux.put("nombre", auxp.getNombre());
//				jaux.put("descripcion", auxp.getDescripcion());
//				jaux.put("foto", auxp.getFoto());
//				jaux.put("anio", auxp.getAnio());
//				jaux.put("director", auxp.getDirector());
//				jaux.put("id", auxp.getId());
//				
//				
//				jugArray.put(jaux);
//				
//				obj.put("error", 0);
//				obj.put("peliculas", jugArray);
//				
//			}
//			
//		}else{
//			obj.put("error", 1);
//			obj.put("message", "El estudio no tiene peliculas.");
//		}
//
//		return ResponseEntity.ok().body(obj.toString());
//	
//	}
	
	
	//muestroestudios MAV
	@GetMapping(path = "/studios")
	public ModelAndView mostrarEstudios(HttpServletRequest request) {
		
		List<Estudio> estudiosFound = estudiodao.findAll();
		
		ModelAndView mav = new ModelAndView("estudios"); // html
		
		mav.addObject("estudios", estudiosFound);
	
		return mav;
	}
	
	//muestro peliculas MAV
	@GetMapping(path = "/films")
	public ModelAndView mostrarPeliculas(HttpServletRequest request) {
		
		List<Pelicula> peliculasFound = pelidao.findAll();
		
		ModelAndView mav = new ModelAndView("peliculas"); // html
		
		mav.addObject("peliculas", peliculasFound);
	
		return mav;
	}


	//estudio MAV
	@GetMapping("/estudio/{id}")
	public ModelAndView verEstudio(@PathVariable("id") Long idest) {

		Estudio stud = estudiodao.findById(idest).orElse(null);
		
		ModelAndView mav = new ModelAndView("estudio"); //html
		
		if(stud != null) {
			
			mav.addObject("id", stud.getId());
			mav.addObject("nombre", stud.getNombre());
			mav.addObject("foto", stud.getFoto());
			mav.addObject("anio", stud.getAnio());
			mav.addObject("pais", stud.getPais());
			mav.addObject("presidente", stud.getPresidente());	
			
		} else {
			
			mav.addObject("no se encontraron datos del estudio");
		}
		
	    return mav;
	}
	
	
	//MAV pelicula de x estudio (paso el id del estudio)
	@GetMapping("filmsestudio/{id}")
	ModelAndView verPeliculas(@PathVariable ("id") long idest) {
		
		Estudio est = estudiodao.findById(idest).orElse(null);
	
		ModelAndView mav = new ModelAndView("peliculas"); //html

		if(est!=null) {

			List<Pelicula> peliculasFound = pelidao.findByEstudio(est);

			if(peliculasFound.size()>0) {

				for(Pelicula peli: peliculasFound) {

					mav.addObject("nombre", peli.getNombre());
					mav.addObject("imagen", peli.getFoto());
					mav.addObject("anio", peli.getAnio());

				}
				
				
			}else {
				mav.addObject("no se encontraron peliculas");
			}
		
		}else {
			mav.addObject("empresa", est.getNombre());
			mav.addObject("foto", est.getFoto());
		}
		return mav;
	}

	//pelicula MAV
	@GetMapping("/peliculas/{id}")
	public ModelAndView verPelicula(@PathVariable("id") Long idpeli) {
		
		Pelicula peli = pelidao.findById(idpeli).orElse(null);
		
		ModelAndView mav = new ModelAndView("pelicula"); //html
		
		if(peli != null) {
			
			mav.addObject("nombre", peli.getNombre());
			mav.addObject("foto", peli.getFoto());
			mav.addObject("anio", peli.getAnio());
			mav.addObject("descripcion", peli.getDescripcion());
			mav.addObject("director", peli.getDirector());
			mav.addObject("id", peli.getId());
			
		} else {
			
			mav.addObject("no se encontraron datos de la pelicula");
		}
		
	    return mav;
	}
	
	//eliminar estudios json
	@GetMapping(path = {"/delete/{id}"})
	public ResponseEntity <Object> eliminarEst(@PathVariable ("id") long idest, HttpServletRequest request){
		
		Usuario user = new Usuario();
		
		Estudio study = estudiodao.findById(idest).orElse(null);
		JSONObject objdel = new JSONObject();
		
		
//		if(user.getNombre().toString().equals("sofia")) {
			
			if(study!=null) {
			
				study.getId();
				estudiodao.delete(study);
				
				
				objdel.put("error", 0);
				objdel.put("message", "Estudio eliminado con exito");
				
			}else {
				objdel.put("error", 1);
				objdel.put("message", "No se encontro el estudio");
			}
			
			
//		}else {
//			objdel.put("error", 1);
//			objdel.put("message", "No tiene acceso a esta funcion");
//	
			return ResponseEntity.ok().body(objdel.toString());

		}
		
	

	//eliminar peliculas json
		@GetMapping(path = {"/deletefilm/{id}"})
		public ResponseEntity <Object> eliminarFilm(@PathVariable ("id") long idpeli, HttpServletRequest request){
			Usuario user = new Usuario();
			
			Pelicula peli = pelidao.findById(idpeli).orElse(null);
			JSONObject objdel = new JSONObject();
			
			
//			if(user.getNombre().toString().equals("sofia")) {
				
				if(peli!=null) {
				
					peli.getId();
					pelidao.delete(peli);
					
					
					objdel.put("error", 0);
					objdel.put("message", "Pelicula eliminada con exito");
					
				}else {
					objdel.put("error", 1);
					objdel.put("message", "No se encontro la pelicula");
				}
				
				
//			}else {
//				objdel.put("error", 1);
//				objdel.put("message", "No tiene acceso a esta funcion");
//				
//			}
			
			return ResponseEntity.ok().body(objdel.toString());
		}


		//eliminar estudios MAV
		@GetMapping(path = "/deleteEstudio/{id}")
		public ModelAndView eliminarEstudio(@PathVariable ("id") Long idest) {
			
			Estudio estu = estudiodao.findById(idest).orElse(null);
			ModelAndView mav = new ModelAndView("eliminar"); //html
			
			if(estu!=null) {
				estudiodao.deleteById(idest);
				mav.addObject("estudio eliminado");
			
			}else {
				mav.addObject("no se ha podido eliminar el estudio");
	
			}
			return mav;
		}
		
		
//		//eliminar pelicula MAV
//		@GetMapping(path = "{idest}/deleteFilm/{id}")
//		public ModelAndView eliminarPelicula(@PathVariable ("id") Long idest, @PathVariable("id") Long idfilm) {
//			
//			Estudio estu = estudiodao.findById(idest).orElse(null);
//			
//			ModelAndView mav = new ModelAndView("eliminarfilm"); //html
//			
//			if(estu!= null) {
//				
//				List<Pelicula> pelis = pelidao.findByEstudio(estu);
//			
//			}
//			
//		}
		
		
		
}		