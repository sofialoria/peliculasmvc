package com.accenture.peliculas.model;

public class InputPelicula {

	
	private Long idEstudio;
	private Pelicula peli;
	
	
	public Long getIdEstudio() {
		return idEstudio;
	}
	public void setIdEstudio(Long idEstudio) {
		this.idEstudio = idEstudio;
	}
	public Pelicula getPeli() {
		return peli;
	}
	public void setPeli(Pelicula peli) {
		this.peli = peli;
	}
	
}
