package com.accenture.peliculas.model;

import java.util.List;
import javax.persistence.*;


@Entity
@Table(name= "estudios")
public class Estudio {


	@Id
	@GeneratedValue
	Long Id;

	String nombre;
	String anio;
	String presidente;
	String pais;
	String foto;


	@OneToMany(cascade = CascadeType.ALL,
			mappedBy = "estudio")
			private List<Pelicula> peliculas;


	public List<Pelicula> getPeliculas() {
		return peliculas;
	}
	public void setPeliculas(List<Pelicula> peliculas) {
		this.peliculas = peliculas;
	}
	public void setAnio(String anio) {
		this.anio = anio;
	}
	public Long getId() {
		return Id;
	}
	public void setId(Long id) {
		Id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getAnio() {
		return anio;
	}
	public void setFundacion(String anio) {
		this.anio = anio;
	}
	public String getPresidente() {
		return presidente;
	}
	public void setPresidente(String presidente) {
		this.presidente = presidente;
	}
	public String getPais() {
		return pais;
	}
	public void setPais(String pais) {
		this.pais = pais;
	}
	public String getFoto() {
		return foto;
	}
	public void setFoto(String foto) {
		this.foto = foto;
	}



}