package com.accenture.peliculas.model;

import javax.persistence.*;


@Entity
@Table(name = "peliculas")
public class Pelicula {

	
	@Id
	@GeneratedValue
	Long Id;
	String nombre;
	String descripcion;
	String anio;
	String director;
	String foto;
	
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "estudio_id")
		private Estudio estudio;
	
	
	public Long getId() {
		return Id;
	}
	public void setId(Long id) {
		Id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getAnio() {
		return anio;
	}
	public void setAnio(String anio) {
		this.anio = anio;
	}
	public String getDirector() {
		return director;
	}
	public void setDirector(String director) {
		this.director = director;
	}
	public String getFoto() {
		return foto;
	}
	public void setFoto(String foto) {
		this.foto = foto;
	}
	public Estudio getEstudio() {
		return estudio;
	}
	public void setEstudio(Estudio estudio) {
		this.estudio = estudio;
	}

}