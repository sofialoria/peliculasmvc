package com.accenture.peliculas.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.accenture.peliculas.model.Usuario;

public interface DaoUser extends JpaRepository<Usuario, Long> {

}
