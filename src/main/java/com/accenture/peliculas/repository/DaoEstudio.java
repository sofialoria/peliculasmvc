package com.accenture.peliculas.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.accenture.peliculas.model.Estudio;

public interface DaoEstudio extends JpaRepository<Estudio, Long> {

}
