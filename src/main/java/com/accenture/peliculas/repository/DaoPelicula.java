package com.accenture.peliculas.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.accenture.peliculas.model.Estudio;
import com.accenture.peliculas.model.Pelicula;

public interface DaoPelicula extends JpaRepository<Pelicula, Long> {

	List<Pelicula> findByEstudio(Long idest);

	List<Pelicula> findByEstudio(Estudio est);

	@Override
	default Optional<Pelicula> findById(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	default void deleteById(Long id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	default List<Pelicula> findAllById(Iterable<Long> ids) {
		// TODO Auto-generated method stub
		return null;
	}

}
